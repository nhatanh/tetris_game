#!/usr/bin/env ruby

require_relative 'lib/bot/constant_define'
require_relative 'lib/bot/command'
require_relative 'lib/bot/formatter'
require_relative 'lib/bot/game'
require_relative 'lib/bot/state'
require_relative 'lib/bot/settings'
require_relative 'lib/bot/bot'
require_relative 'lib/bot/player'
require_relative 'lib/pieces/piece'
require_relative 'lib/pieces/z'
require_relative 'lib/pieces/s'
require_relative 'lib/pieces/o'
require_relative 'lib/pieces/i'
require_relative 'lib/pieces/l'
require_relative 'lib/pieces/j'
require_relative 'lib/pieces/t'

def main
  $stdout.sync = true # sets up immediate output flush
  Bot.new.run
end

main

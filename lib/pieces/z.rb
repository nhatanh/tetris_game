class Piece_Z < Piece

	def self.weighted_score(field, base_line, holes)
    holes.map do |hole|
      hole[:score] = 0
      if hole[:size] == 2 && clear_from_top(hole[:position], 2, base_line, field)
        if self.free_top_left(hole, base_line, field)
          hole[:score] = 4 - space_under(hole[:position], base_line, field) - space_under(hole[:position] + 1, base_line, field)
        end
        if self.free_top_right(hole, base_line, field)
          score = 2 - space_under(hole[:position], base_line, field)
          hole[:score] = score if score > hole[:score]
        end
      elsif hole[:size] > 2 && has_clearance(hole, base_line, field)
        if self.free_top_left(hole, base_line, field)
          hole[:score] = 3 - space_under(hole[:position], base_line, field) - space_under(hole[:position] + 1, base_line, field)
        end
        if self.free_top_right(hole, base_line, field)
          score = 2 - space_under(hole[:position] + hole[:size] - 1, base_line, field)
          hole[:score] = score if score > hole[:score]
        else
          hole[:score] = 1
        end
      elsif hole[:size] == 1 && self.free_top_right(hole, base_line, field) && clear_from_top(hole[:position], 1, base_line, field)
        hole[:score] = 3 - space_under(hole[:position], base_line, field)
      end
      hole
    end.sort { |a, b| a[:score] <=> b[:score] }.reverse
  end

	def self.final_position(field, base_line, hole)
    return { x: base_line - 2, y: hole[:position], turn: 90 } if hole[:size] == 1
    if self.free_top_left(hole, base_line, field)
      return { x: base_line - 1, y: hole[:position] - 1, turn: 0 }
    elsif self.free_top_right(hole, base_line, field)
      return { x: base_line - 2, y: hole[:position] + hole[:size] - 1, turn: 90 }
    else
      if has_left_side(hole, base_line, field)
        return { x: base_line - 1, y: hole[:position] + 1, turn: 0 }
      end
      hole_pos = self.hole_bottom_right(hole, base_line, field)
      return { x: base_line - 1, y: hole_pos - 3, turn: 0 }
    end
  end
end
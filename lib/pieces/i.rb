class Piece_I < Piece

  def self.weighted_score(field, base_line, holes)
    holes.map do |hole|
      hole[:score] = 0
      if hole[:size] > 3 && has_clearance(hole, base_line, field)
        hole[:score] = 6
      else
        max_point = 0
        (hole[:position]..(hole[:position] + hole[:size] - 1)).each do |i|
          point = get_weight_score_column(base_line, i, field)
          max_point = point if point > max_point && clear_from_top(i, 1, base_line, field)
        end
        hole[:score] = max_point
      end
      hole
    end.sort { |a, b| a[:score] <=> b[:score] }.reverse
  end

  def self.get_weight_score_column(base_line, position, field)
    return 0 if base_line < 4
    point = 0
    4.times do
      point += 1 if position + 1 == field[0].length || field[base_line][position + 1] == '2'
      point += 1 if position.zero? || field[base_line][position - 1] == '2'
      return 0 if field[base_line][position] == '2'
      base_line -= 1
    end
    return point
  end

  def self.final_position(field, base_line, hole)
    if hole[:size] == 4
      return { x: base_line - 1, y: hole[:position], turn: 0 } if clear_from_top(hole[:position], 4, base_line, field)
      (0..3).each do |i|
        return { x: base_line - 3, y: hole[:position] - 1, turn: 90 } if clear_from_top(hole[:position] + i, 1, base_line, field)
      end
    end
    if hole[:size] > 4
      if has_left_side(hole, base_line, field) && clear_from_top(hole[:position] + 1, 4, base_line, field)
        return { x: base_line - 1, y: hole[:position], turn: 0 }
      elsif has_right_side(hole, base_line, field) && clear_from_top(hole[:position] + hole[:size] - 5, 4, base_line, field)
        return { x: base_line - 1, y: hole[:position] + hole[:size] - 4, turn: 0 }
      end
      hole_pos = hole[:position]
      hole_end = hole[:position] + hole[:size]
      return { x: base_line - 1, y: hole_pos, turn: 0 } if base_line + 1 == field.length && clear_from_top(hole[:position], 4, base_line, field)
      while hole_pos + 4 < hole_end && (base_line + 1 == field.length || field[base_line + 1][hole_pos] != '2' || field[base_line + 1][hole_pos + 1] != '2' ||
            field[base_line + 1][hole_pos + 2] != '2' || field[base_line + 1][hole_pos + 3] != '2') && !clear_from_top(hole_pos, 4, base_line, field)
        hole_pos += 1
      end
      return { x: base_line - 1, y: hole_pos, turn: 0 } if hole_pos < hole_end && clear_from_top(hole_pos, 4, base_line, field)
    end
    max_point = 0
    max_index = hole[:position]
    (hole[:position]..(hole[:position] + hole[:size] - 1)).each do |i|
      point = get_weight_score_column(base_line, i, field)
      if point > max_point
        max_index = i
        max_point = point
      end
    end
    { x: base_line - 3, y: max_index - 1, turn: 90 }
  end
end
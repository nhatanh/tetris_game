class Piece_T < Piece

  def self.weighted_score(field, base_line, holes)
    holes.map do |hole|
      hole[:score] = 0
      if hole[:size] == 1 && clear_from_top(hole[:position], 1, base_line, field)
        if fit_t_center(hole, base_line, field)
          hole[:score] = 5 - space_under(hole[:position], base_line, field)
        end
        if (fit_t_left(hole, base_line, field) || fit_t_right(hole, base_line, field))
          score = 4 - space_under(hole[:position], base_line, field)
          hole[:score] = score if score > hole[:score]
        end
      elsif hole[:size] == 2
        if fit_t_left(hole, base_line, field) && clear_from_top(hole[:position], 1, base_line, field)
          hole[:score] = 4 - space_under(hole[:position], base_line, field)
        end
        if fit_t_right(hole, base_line, field) && clear_from_top(hole[:position] + 1, 1, base_line, field)
          score = 4 - space_under(hole[:position] + 1, base_line, field)
          hole[:score] = score if score > hole[:score]
        end
      elsif hole[:size] == 3 && clear_from_top(hole[:position], 3, base_line, field)
        hole[:score] = 6 - space_under(hole[:position], base_line, field) - space_under(hole[:position] + 1, base_line, field) - space_under(hole[:position] + 2, base_line, field)
      elsif hole[:size] > 3 && has_clearance(hole, base_line, field)
        hole[:score] = 6 - space_under(hole[:position], base_line, field) - space_under(hole[:position] + 1, base_line, field) - space_under(hole[:position] + 2, base_line, field)
      end
      hole
    end.sort { |a, b| a[:score] <=> b[:score] }.reverse
  end

  # _xxx__
  #  |x|
  
  def self.fit_t_center(hole, base_line, field)
    position = hole[:position]
    position > 0 && position < field[0].length - 1 && base_line > 0 &&
      field[base_line][position - 1] == '2' && field[base_line][position + 1] == '2' &&
      field[base_line - 1][position - 1] != '2' && field[base_line - 1][position + 1] != '2' &&
      field[base_line - 1][position] != '2'
  end
  #   x
  # _xx   __
  #  |x__|
  
  def self.fit_t_left(hole, base_line, field)
    position = hole[:position]
    position > 0 && base_line > 1 &&
      field[base_line][position - 1] == '2' &&
      field[base_line - 1][position - 1] != '2' && field[base_line - 1][position] != '2' &&
      field[base_line - 2][position - 1] != '2' && field[base_line - 2][position] != '2'
  end
  #     x
  # _   xx__
  #  |__x|
  
  def self.fit_t_right(hole, base_line, field)
    position = hole[:position] + hole[:size] - 1
    position < field[0].length - 1 && base_line > 1 &&
      field[base_line][position + 1] == '2' &&
      field[base_line - 1][position + 1] != '2' && field[base_line - 1][position] != '2' &&
      field[base_line - 2][position + 1] != '2' && field[base_line - 2][position] != '2'
  end

  def self.final_position(field, base_line, hole)
    position = hole[:position] + hole[:size] - 1
    if hole[:size] == 1
      if fit_t_center(hole, base_line, field)
        return { x: base_line - 2, y: hole[:position] - 1, turn: 180 }
      elsif fit_t_left(hole, base_line, field)
        return { x: base_line - 2, y: hole[:position] - 1, turn: 90 }
      else
        return { x: base_line - 2, y: hole[:position] - 1, turn: -90 }
      end
    end
    if hole[:size] == 2
      score_left = -10
      score_left = 0 - space_under(hole[:position], base_line, field) if fit_t_left(hole, base_line, field)
      score_right = -10
      score_right = 0 - space_under(hole[:position] + 1, base_line, field) if fit_t_right(hole, base_line, field)
      if score_left > score_right
        return { x: base_line - 2, y: hole[:position] - 1, turn: 90 }
      else
        return { x: base_line - 2, y: hole[:position], turn: -90 }
      end
    end
    
    if has_left_side(hole, base_line, field)
      return { x: base_line - 1, y: hole[:position], turn: 0 }
    elsif has_right_side(hole, base_line, field)
      return { x: base_line - 1, y: hole[:position] + hole[:size] - 3, turn: 0 }
    end

    position_score = []
    (hole[:position]..(hole[:position] + hole[:size] - 3)).each do |pos|
      position_score.push(score: 0 - space_under(pos, base_line, field) - space_under(pos + 1, base_line, field) - space_under(pos + 2, base_line, field), position: pos)
    end
    position_score = position_score.sort { |a, b| a[:score] <=> b[:score] }.reverse
    { x: base_line - 1, y: position_score[0][:position], turn: 0 }
  end
end
class Piece_O < Piece

  def self.weighted_score(field, base_line, holes)
    holes.map do |hole|
      hole[:score] = 0
      if hole[:size] == 2 && clear_from_top(hole[:position], 2, base_line, field)
        hole[:score] = 4 - space_under(hole[:position], base_line, field) - space_under(hole[:position] + 1, base_line, field)
      elsif hole[:size] > 2 && has_clearance(hole, base_line, field)
        hole[:score] = 3
      end
      hole
    end.sort { |a, b| a[:score] <=> b[:score] }.reverse
  end

  def self.final_position(field, base_line, hole)
    return { x: base_line - 1, y: hole[:position], turn: 0 } if hole[:size] == 2
    hole_pos = hole[:position]
    hole_end = hole[:position] + hole[:size]
    return { x: base_line - 1, y: hole_pos, turn: 0 } if base_line + 1 == field.length
    position_score = []
    (hole_pos..(hole_end - 2)).each do |pos|
      position_score.push(score: 0 - space_under(pos, base_line, field) - space_under(pos + 1, base_line, field), position: pos)
    end
    position_score = position_score.sort { |a, b| a[:score] <=> b[:score] }.reverse
    
    { x: base_line - 1, y: position_score[0][:position], turn: 0 }
  end
end
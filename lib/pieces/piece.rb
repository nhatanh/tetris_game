class Piece

	def self.weighted_score(field, base_line, holes)
=begin
field = [
  ['0','0','0','0','0','0','0','0','0','0','0'],
  ['0','0','0','0','0','0','0','1','0','0','0'],
  ['0','0','0','0','0','0','1','1','0','0','0'],
  ['0','0','0','0','0','0','1','0','0','0','0'],
  ['0','0','0','0','0','0','0','0','0','0','0'],
  ['2','2','2','2','2','2','0','2','2','2','2'],
]
base_line (integer): vị trí hàng mà mình đang xem xét các lỗ
holes: vị trí và kích cỡ các lỗ [{position: 3, size: 1}, {position: 5, size: 3}, {position: 9, size: 2}]

=====>output: thêm điểm cho từng lỗ và sắp xếp theo điểm giảm dần
[{position: 9, size: 2, score: 4}, {position: 5, size: 3, score: 2}, {position: 3, size: 1, score: 1}]
=end
    holes.map do |hole|
      hole[:score] = 0
      hole
    end.sort { |a| a[:score] }
  end

=begin
hole: là lỗ mình quyết định chọn đặt piece này vào. {position: 2, size: 5}
Bây giờ cần phải quyết định đặt nó vào vị trí nào
=====>output: { x: hàng, y: cột, turn: 90|180|270}
=end
	def self.final_position(field, base_line, hole)
    return { x: base_line - 1, y: hole[:position] - 1, turn: 0 }
  end

  #______X           _______
  #      |__________|
  def self.free_top_left(hole, base_line, field, count = 1)
    return false if base_line.zero?
    position = hole[:position] - 1
    (1..count).each do ||
      return false if position < 0 || field[base_line - 1][position] == '2' || field[base_line][position] != '2'
      position -= 1
    end
    true
  end

  #_______          X_______
  #      |__________|
  def self.free_top_right(hole, base_line, field, count = 1)
    return false if base_line.zero?
    position = hole[:position] + hole[:size]
    (1..count).each do ||
      return false if position >= field[0].length || field[base_line - 1][position] == '2' || field[base_line][position] != '2'
      position += 1
    end
    true
  end

  #_______          _______
  #      |________  |
  def self.hole_bottom_right(hole, base_line, field)
    hole_pos = hole[:position] + hole[:size]
    return hole_pos if base_line == field.length - 1
    hole_pos -= 1 while field[base_line + 1][hole_pos - 1] != '2'
    hole_pos # where the hole starts
  end

  #_______          _______
  #      |  ________|
  def self.hole_bottom_left(hole, base_line, field)
    hole_pos = hole[:position] - 1
    return hole_pos if base_line == field.length - 1
    hole_pos += 1 while field[base_line + 1][hole_pos + 1] != '2'
    hole_pos # where the hole starts
  end

  def self.clear_from_top(position, length, to_line, field)
    (0..to_line).each do |line|
      (1..length).each do |row|
        a = position + row - 1
        return false if a < 0 || a > field[0].length || field[line][a] == '2'
      end
    end
    true
  end

  def self.has_left_side(hole, base_line, field)
    field[base_line - 1][hole[:position]] == '2'
  end

  def self.has_right_side(hole, base_line, field)
    field[base_line - 1][hole[:position] + hole[:size] - 1] == '2'
  end

  def self.has_clearance(hole, base_line, field)
    begin_hole = hole[:position]
    end_hole = hole[:position] + hole[:size] - 1
    begin_hole += 1 if has_left_side(hole, base_line, field)
    end_hole -= 1 if has_right_side(hole, base_line, field)
    return false if begin_hole > end_hole
    clear_from_top(begin_hole, end_hole + 1 - begin_hole, base_line, field)
  end

  def self.can_slide_in_right(piece_type, turn)
    case piece_type
    when I
      return turn == 0 || turn == 180
    when J
      return turn == 0
    when L
      return turn == -90 || turn == 270
    when O
      return false
    when S
      return false
    when T
      return turn == 0
    when Z
      return turn == 0 || turn == 180
    end
  end

  def self.can_slide_in_left(piece_type, turn)
    case piece_type
    when I
      return turn == 0 || turn == 180
    when J
      return turn == 90
    when L
      return turn == 0
    when O
      return false
    when S
      return turn == 0 || turn == 180
    when T
      return turn == 0
    when Z
      return false
    end
  end

  def self.space_under(position, base_line, field)
    count = 0
    if field[base_line + 1] && field[base_line + 1][position] == '0'
      count += 5
    else
      return count
    end
    if field[base_line + 2] && field[base_line + 2][position] == '0'
      count += 3
    else
      return count
    end
    if field[base_line + 3] && field[base_line + 3][position] == '0'
      count += 1
    else
      return count
    end
    if field[base_line + 4] && field[base_line + 4][position] == '0'
      count += 1
    end

    if field[base_line + 5] && field[base_line + 5][position] == '0'
      count -= 5
    end

    if field[base_line + 6] && field[base_line + 6][position] == '0'
      count -= 5
    end
    count
  end
end
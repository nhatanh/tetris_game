class Piece_J < Piece

  def self.weighted_score(field, base_line, holes)
    holes.map do |hole|
      hole[:score] = 0
      if hole[:size] == 1 && clear_from_top(hole[:position], 1, base_line, field)
        if free_top_left(hole, base_line, field, 2)
          hole[:score] = 5 - space_under(hole[:position], base_line, field)
        end
        if fit_j(hole, base_line, field)
          score = 4 - space_under(hole[:position], base_line, field)
          hole[:score] = score if score > hole[:score]
        end
      elsif hole[:size] == 2 && clear_from_top(hole[:position], 2, base_line, field)
        if free_top_left(hole, base_line, field, 2)
          hole[:score] = 4 - space_under(hole[:position], base_line, field)
        end
        score = 3 - space_under(hole[:position], base_line, field) - space_under(hole[:position] + 1, base_line, field)
        hole[:score] = score if score > hole[:score]
      elsif hole[:size] > 2 && has_clearance(hole, base_line, field)
        if free_top_left(hole, base_line, field, 2)
          hole[:score] = 5 - space_under(hole[:position], base_line, field)
        end
        if fit_j(hole, base_line, field)
          score = 4 - space_under(hole[:position] + hole[:size] - 1, base_line, field)
          hole[:score] = score if score > hole[:score]
        end
        if hole[:score] < 1
          hole[:score] = 5 - space_under(hole[:position], base_line, field) - space_under(hole[:position] + 1, base_line, field) - space_under(hole[:position] + 2, base_line, field)
        end
      end
      hole
    end.sort { |a, b| a[:score] <=> b[:score] }.reverse
  end

  #__     xx__
  #  |    x|
  #  |____x|
  def self.fit_j(hole, base_line, field)
    position = hole[:position] + hole[:size] - 1
    base_line - 1 > -1 && field[base_line - 1][position] != '2' &&
      position + 1 < field[0].length && field[base_line - 1][position + 1] == '2' &&
      base_line - 2 > -1 && field[base_line - 2][position] != '2' &&
      field[base_line - 2][position + 1] != '2'
  end

  def self.final_position(field, base_line, hole)
    position = hole[:position] + hole[:size] - 1
    if hole[:size] == 1
      if free_top_left(hole, base_line, field, 2)
        return { x: base_line - 2, y: hole[:position] - 2, turn: 180 }
      else # must be fit_j
        return { x: base_line - 2, y: hole[:position] - 1, turn: -90 }
      end
    end
    if hole[:size] == 2
      if free_top_left(hole, base_line, field, 2)
        return { x: base_line - 2, y: hole[:position] - 2, turn: 180 }
      elsif fit_j(hole, base_line, field)
        return { x: base_line - 2, y: hole[:position] + hole[:size] - 2, turn: -90 }
      else
        return { x: base_line - 2, y: hole[:position], turn: 90 }
      end
    end
    
    if has_left_side(hole, base_line, field)
      return { x: base_line - 2, y: hole[:position], turn: 90 }
    elsif has_right_side(hole, base_line, field)
      return { x: base_line - 1, y: hole[:position] + hole[:size] - 3, turn: 0 }
    end

    if free_top_left(hole, base_line, field, 2)
      return { x: base_line - 2, y: hole[:position] - 2, turn: 180 }
    elsif fit_j(hole, base_line, field)
      return { x: base_line - 2, y: hole[:position] + hole[:size] - 2, turn: -90 }
    end

    position = hole[:position]
    (hole[:position]..(hole[:position] + hole[:size] - 3)).each do |i|
      position = i
      next_row = base_line + 1
      break if next_row == field.length
      if field[next_row][position] == '2' && field[next_row][position + 1] == '2' &&
         field[next_row][position + 2] == '2'
        break
      end
    end
    { x: base_line - 1, y: position, turn: 0 }
  end
end
class Game
  attr_accessor :state, :action

  def initialize(current_state)
    @state = current_state
    @action = Command.new
  end

  def caculate_next_action
    fit_through_hole = 2
    fit_through_hole = 1 if @state.this_piece_type == I || @state.this_piece_type == L || @state.this_piece_type == J
    base_line = find_base_line(fit_through_hole)
    piece = get_piece
    previous_holes = [[score: 0]]
    while true
      holes = detect_hole(@state.players[0].field[base_line])
      holes = piece.weighted_score(@state.players[0].field, base_line, holes)
      if (holes.empty? || holes[0][:score] <= 0) && can_still_look_up(holes[0], base_line, @state.players[0].field)
        base_line -= 1
        break if base_line < 0
        previous_holes = holes
        next
      end
      if holes[0][:score] <= 0
        base_line += 1
        holes = previous_holes
      end
      final_position = piece.final_position(@state.players[0].field, base_line, holes[0])
      #STDOUT.puts base_line
      #STDOUT.puts holes
      #STDOUT.puts final_position
      generate_step(final_position, base_line, holes[0], @state.players[0].field)
      break
    end
  end

  def get_piece
    case @state.this_piece_type
    when I
      Piece_I
    when J
      Piece_J
    when L
      Piece_L
    when O
      Piece_O
    when S
      Piece_S
    when T
      Piece_T
    when Z
      Piece_Z
    end
  end

  def generate_step(final_position, base_line, hole, field)
    current_position = @state.this_piece_position.split(',').map(&:to_i)
    steps = []
    if final_position[:turn] == 90
      steps.push(:turnleft)
    end
    if final_position[:turn] == 180
      steps.push(:turnleft)
      steps.push(:turnleft)
    end
    if final_position[:turn] == -90 || final_position[:turn] == 270
      steps.push(:turnright)
    end
    if final_position[:y] > current_position[0]
      (final_position[:y] - current_position[0] - 1).times.each { |_| steps.push(:right) }
      if Piece.has_left_side(hole, base_line, field)
        steps.push(:right)
        if Piece.can_slide_in_left(@state.this_piece_type, final_position[:turn])
          steps.push(:right)
          (final_position[:x] - current_position[1]).times.each { |_| steps.push(:down) } 
          steps.push(:left)
        end
      elsif Piece.has_right_side(hole, base_line, field)
        if Piece.can_slide_in_right(@state.this_piece_type, final_position[:turn])
          (final_position[:x] - current_position[1]).times.each { |_| steps.push(:down) }
        end
        steps.push(:right)
      else
        steps.push(:right)
      end
    elsif final_position[:y] < current_position[0]
      (current_position[0] - final_position[:y] - 1).times.each { |_| steps.push(:left) }
      if Piece.has_left_side(hole, base_line, field)
        if Piece.can_slide_in_left(@state.this_piece_type, final_position[:turn])
          (final_position[:x] - current_position[1]).times.each { |_| steps.push(:down) }
        end
        steps.push(:left)
      elsif Piece.has_right_side(hole, base_line, field)
        steps.push(:left)
        if Piece.can_slide_in_right(@state.this_piece_type, final_position[:turn])
          steps.push(:left)
          (final_position[:x] - current_position[1]).times.each { |_| steps.push(:down) }
          steps.push(:right)
        end
      else
        steps.push(:left)
      end
      
    else # drop down directly
      if Piece.has_left_side(hole, base_line, field)
        if Piece.can_slide_in_left(@state.this_piece_type, final_position[:turn])
          steps.push(:right)
          (final_position[:x] - current_position[1]).times.each { |_| steps.push(:down) }
          steps.push(:left)
        end
      elsif Piece.has_right_side(hole, base_line, field)
        if Piece.can_slide_in_right(@state.this_piece_type, final_position[:turn])
          steps.push(:left)
          (final_position[:x] - current_position[1]).times.each { |_| steps.push(:down) }
          steps.push(:right)
        end
      end
    end
    
    puts steps.join(',')
  end

  def find_base_line(fit_through_hole)
    base_line = 0
    @state.players[0].field.each do |row|
      holes = detect_hole(row)
      return base_line - 1 if holes.empty?
      if holes.select { |hole| hole[:size] >= fit_through_hole }.empty?
        return base_line
      end
      base_line += 1
    end
    return base_line-1
  end

  def detect_hole(row)
    is_wall = true
    holes = []
    last_position = 0
    row.each_with_index do |cell, i|
      next unless is_wall ^ (cell != '0' && cell != '1')
      is_wall = !is_wall
      unless is_wall
        last_position = i
        next
      end
      holes.push(position: last_position, size: i - last_position)
    end
    unless is_wall
      holes.push(position: last_position, size: row.length - last_position)
    end
    holes
  end

  def can_still_look_up(hole, base_line, field)
    return false if base_line == 0
    hole[:size] != field[0].length
  end
end
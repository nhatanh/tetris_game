#!/usr/bin/env ruby

require_relative '../lib/bot/constant_define'
require_relative '../lib/bot/command'
require_relative '../lib/bot/formatter'
require_relative '../lib/bot/game'
require_relative '../lib/bot/state'
require_relative '../lib/bot/settings'
require_relative '../lib/bot/bot'
require_relative '../lib/bot/player'
require_relative '../lib/pieces/piece'
require_relative '../lib/pieces/z'
require_relative '../lib/pieces/s'
require_relative '../lib/pieces/o'
require_relative '../lib/pieces/i'
require_relative '../lib/pieces/l'
require_relative '../lib/pieces/j'
require_relative '../lib/pieces/t'
require 'pry'

def test_data
  File.read("#{File.dirname(__FILE__)}/input.txt")
end

def main
  test_cases = [
    { file: 'z_cases/1.txt', expected: "#{Array.new(2, 'right').join(',')}" },
    { file: 'z_cases/2.txt', expected: "#{Array.new(4, 'right').join(',')}" },
    { file: 'z_cases/3.txt', expected: "#{Array.new(1, 'right').join(',')}" },
    { file: 'z_cases/4.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(2, 'left').join(',')}" },
    { file: 'z_cases/5.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(2, 'right').join(',')}" },
    { file: 's_cases/1.txt', expected: "#{Array.new(2, 'right').join(',')}" },
    { file: 's_cases/2.txt', expected: "#{Array.new(4, 'left').join(',')}" },
    { file: 's_cases/3.txt', expected: "#{Array.new(1, 'right').join(',')}" },
    { file: 's_cases/4.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(5, 'left').join(',')}" },
    { file: 's_cases/5.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 'o_cases/1.txt', expected: "#{Array.new(2, 'right').join(',')}" },
    { file: 'o_cases/2.txt', expected: "#{Array.new(4, 'left').join(',')}" },
    { file: 'o_cases/3.txt', expected: "#{Array.new(1, 'right').join(',')}" },
    { file: 'o_cases/4.txt', expected: "#{Array.new(2, 'left').join(',')}" },
    { file: 'o_cases/5.txt', expected: "" },
    { file: 'i_cases/1.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(1, 'right').join(',')}" },
    { file: 'i_cases/2.txt', expected: "#{Array.new(2, 'right').join(',')}" },
    { file: 'i_cases/3.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(3, 'right').join(',')}" },
    { file: 'i_cases/4.txt', expected: "#{Array.new(3, 'left').join(',')}" },
    { file: 'i_cases/5.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(1, 'right').join(',')}" },
    { file: 'l_cases/1.txt', expected: "" },
    { file: 'l_cases/2.txt', expected: "#{Array.new(4, 'right').join(',')}" },
    { file: 'l_cases/3.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 'l_cases/4.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 'l_cases/5.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 'j_cases/2.txt', expected: "#{Array.new(2, 'turnleft').join(',')},#{Array.new(1, 'right').join(',')}" },
    { file: 'j_cases/3.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(3, 'left').join(',')}" },
    { file: 'j_cases/4.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(3, 'right').join(',')}" },
    { file: 'j_cases/5.txt', expected: "#{Array.new(2, 'turnleft').join(',')},#{Array.new(4, 'right').join(',')}" },
    { file: 'j_cases/1.txt', expected: "#{Array.new(3, 'left').join(',')}" },
    { file: 'z_cases/6.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(1, 'right').join(',')}" },
    #{ file: 'l_cases/6.txt', expected: "#{Array.new(1, 'turnright').join(',')}" },
    { file: 'j_cases/6.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 's_cases/6.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(3, 'left').join(',')}" },
    { file: 'i_cases/6.txt', expected: "#{Array.new(19, 'down').join(',')},#{Array.new(1, 'right').join(',')}" },
    { file: 'i_cases/7.txt', expected: "#{Array.new(19, 'down').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 'j_cases/7.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(18, 'down').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 'j_cases/8.txt', expected: "#{Array.new(1, 'right').join(',')},#{Array.new(19, 'down').join(',')},#{Array.new(1, 'right').join(',')}" },
    { file: 'l_cases/7.txt', expected: "#{Array.new(19, 'down').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 'l_cases/8.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(1, 'right').join(',')},#{Array.new(18, 'down').join(',')},#{Array.new(1, 'right').join(',')}" },
    { file: 't_cases/1.txt', expected: "#{Array.new(19, 'down').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 't_cases/2.txt', expected: "#{Array.new(1, 'right').join(',')},#{Array.new(19, 'down').join(',')},#{Array.new(1, 'right').join(',')}" },
    { file: 's_cases/7.txt', expected: "#{Array.new(19, 'down').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 's_cases/8.txt', expected: "#{Array.new(1, 'right').join(',')}" },
    { file: 'z_cases/7.txt', expected: "" },
    { file: 'z_cases/8.txt', expected: "#{Array.new(1, 'right').join(',')},#{Array.new(19, 'down').join(',')},#{Array.new(1, 'right').join(',')}" },
    { file: 'j_cases/9.txt', expected: "#{Array.new(2, 'turnleft').join(',')},#{Array.new(1, 'right').join(',')}" }, 
    { file: 'i_cases/8.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(1, 'left').join(',')}" }, 
    { file: 'i_cases/9.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(1, 'left').join(',')}" }, 
    { file: 'j_cases/10.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(4, 'left').join(',')}" }, 
    { file: 'i_cases/10.txt', expected: "" }, 
    { file: 't_cases/3.txt', expected: "#{Array.new(1, 'left').join(',')}" }, 
    { file: 'i_cases/11.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(2, 'right').join(',')}" },
    { file: 't_cases/4.txt', expected: "#{Array.new(1, 'turnleft').join(',')},#{Array.new(4, 'right').join(',')}" },
    { file: 'o_cases/7.txt', expected: "#{Array.new(4, 'right').join(',')}" },
    { file: 's_cases/10.txt', expected: "#{Array.new(1, 'turnright').join(',')},#{Array.new(1, 'left').join(',')}" },
    { file: 's_cases/11.txt', expected: "#{Array.new(1, 'right').join(',')}" },
    { file: 'j_cases/11.txt', expected: "#{Array.new(1, 'turnright').join(',')}" },
  ]
  $stdout.sync = true # sets up immediate output flush
  $stdout = StringIO.new
  test_cases.each do |f|
    $stdout.reopen("")
    Bot.new.run(File.read("#{File.dirname(__FILE__)}/test_cases/#{f[:file]}"))
    if $stdout.string.chomp != f[:expected]
      STDOUT.puts "#{f[:file]}\nexpect: #{f[:expected].inspect}\nactual: #{$stdout.string.inspect}"
    end
  end
end

main
